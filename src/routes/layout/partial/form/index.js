import React from 'react';
import {Button, Card, Col, Form, Input, Row} from 'antd';
import withForm from "../../../../contexts/withForm";
import CodeEditor from "../../../../components/CodeEditor";

import './style.scss';

const PartialForm = ({form, data = {}, onSubmit}) => {

    const {getFieldDecorator, validateFields} = form;

    const handleSubmit = (e) => {
        e.preventDefault();

        validateFields(async (err, fields) => {
            if (err) {
                return;
            }

            onSubmit && onSubmit({
              code: fields.code,
              partialName: fields.partialName,
            });
        });
    };

    return (
      <Form onSubmit={handleSubmit} className="partial-form">
        <Row type="flex" justify="space-between" gutter={12}>
          <Col span={18}>
            <Card bordered={false}>

              <Form.Item label="Partial Code">
                {getFieldDecorator('code', {
                  initialValue: data.code,
                })(
                  <CodeEditor/>
                )}
              </Form.Item>

            </Card>
          </Col>
          <Col span={6}>
            <Card bordered={false}>
              <Form.Item label="Partial Name">
                {getFieldDecorator('partialName', {
                  initialValue: data.partialName,
                  rules: [{required: true, message: 'Please input Partial Code!'}],
                })(
                  <Input
                    placeholder="Partial Name"
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" className="save-button">
                  Save
                </Button>
              </Form.Item>
            </Card>
          </Col>
        </Row>
      </Form>
    );
};

export default withForm(PartialForm);
