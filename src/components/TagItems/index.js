import React, {forwardRef, useEffect, useState} from "react";
import _ from "lodash";

import './style.scss';
import {Icon, Input, Tag, Tooltip} from "antd";
import PropTypes from "prop-types";

const TagItems = forwardRef(({value, onChange, closable}, ref) => {

  const [inputRef, setInputRef] = useState(false);
  const [inputVisible, setInputVisible] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const handleClose = removedTag => {
    const tags = _.filter(value, tag => tag !== removedTag);
    onChange(tags);
  };

  const showInput = () => {

    setInputVisible(true);
  };

  const handleInputChange = ({target}) => {
    setInputValue(target.value);
  };

  const handleInputConfirm = () => {

    if (inputValue && _.indexOf(value, inputValue) === -1) {
      setInputVisible(false);
      setInputValue('');
      onChange([...value, inputValue]);
    }
  };

  useEffect(() => {

    if (inputRef) {
      inputRef.focus();
    }

  }, [inputRef]);

  return (
    <div>
      {
        _.map(value, (tag, index) => {
          const isLongTag = tag.length > 20;
          const tagElem = (
            <Tag key={tag} closable={closable} onClose={() => handleClose(tag)}>
              {isLongTag ? `${tag.slice(0, 20)}...` : tag}
            </Tag>
          );
          return isLongTag
            ? (
              <Tooltip title={tag} key={tag}>{tagElem}</Tooltip>
            )
            : (tagElem);
        })
      }
      {inputVisible && (
        <Input
          ref={setInputRef}
          type="text"
          size="small"
          style={{width: 78}}
          value={inputValue}
          onChange={handleInputChange}
          onBlur={handleInputConfirm}
          onPressEnter={handleInputConfirm}
        />
      )}
      {!inputVisible && (
        <Tag onClick={showInput} style={{background: '#fff', borderStyle: 'dashed'}}>
          <Icon type="plus"/> New Tag
        </Tag>
      )}
    </div>
  );
});

TagItems.propTypes = {
  closable: PropTypes.bool,
  value: PropTypes.array,
  onChange: PropTypes.func,
};

TagItems.defaultProps = {
  closable: true,
  onChange: () => {
  }
};

export default TagItems;
