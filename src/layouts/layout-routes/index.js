import MainLayoutRoute from "./MainLayoutRoute";
import SideMenuLayoutRoute from "./SideMenuLayoutRoute";
import CleanLayoutRoute from "./CleanLayoutRoute";


export {CleanLayoutRoute, MainLayoutRoute, SideMenuLayoutRoute}
