
import {Form} from "antd";
import {withRouter} from "react-router-dom";

export default (Component) => Form.create({name: `${new Date().getTime()}`})(withRouter(Component));
