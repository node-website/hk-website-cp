import React, {useEffect, useState} from 'react';
import {Button, Card, Layout, message, Popconfirm, Table} from 'antd';
import {Link} from "react-router-dom";
import AppPageHeader from "../../../components/AppPageHeader";
import api from "../../../services/api";

import './style.scss';

const {Content} = Layout;

const routes = [
    {
        path: '/layouts/partials',
        text: 'Partials',
    },
];

const Partials = () => {

    const [partials, setPartials] = useState([]);

    const getPartials = async () => {
        try {

            const data = await api.get("/partials");

            setPartials(data.partials);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    const onDeletePartials = (id) => async () => {
        try {
            await api.delete(`/partials/${id}`);

            message.success("Delete Success");

            getPartials();
        } catch (e) {
            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPartials();

    }, []);

    return (
        <>
            <AppPageHeader
                breadcrumb={routes}
                title="Layouts"
                subtitle="Partials Editor"
            />

            <Content className="page-content partials">
                <Card
                    bordered={false}
                    title="Partials"
                    extra={
                        <>
                            <Link to="/layouts/partials/add">
                                <Button type="primary" ghost size="small" icon="plus"/>
                            </Link>
                        </>
                    }
                >
                    <Table
                        dataSource={partials}
                        rowKey="_id"
                        size="small"
                        bordered={true}
                    >
                        <Table.Column
                            title="Partial Name"
                            dataIndex="partialName"
                            key="partialName"
                        />
                        <Table.Column
                            title="How To Use"
                            dataIndex="partialName"
                            key="partialNameUse"
                            render={(partialName) => (<code>{`<%- ${partialName}.ejs -%>`}</code>)}
                        />
                        <Table.Column
                            title="Actions"
                            dataIndex="_id"
                            key="_id"
                            width={90}
                            align="center"
                            render={(id) => (
                                <div className="actions">
                                    <Link to={`/layouts/partials/${id}/edit`}>
                                        <Button type="primary" ghost size="small" icon="edit"/>
                                    </Link>
                                    <Popconfirm
                                        placement="bottomRight"
                                        title="Delete this content?"
                                        onConfirm={onDeletePartials(id)}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <Button type="danger" ghost size="small" icon="delete"/>
                                    </Popconfirm>
                                </div>
                            )}
                        />
                    </Table>
                </Card>
            </Content>
        </>
    );
};

export default Partials;
