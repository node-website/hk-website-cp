import React, {useEffect, useState} from 'react';
import './style.scss';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import PartialForm from "../form";
import api from "../../../../services/api";
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
  {
    path: '/layouts/partials',
    text: 'Partials',
  },
];

const EditPartial = ({match, history}) => {

    const {id} = match.params;
  const [partial, setPartial] = useState();

    const getPartial = async () => {
        try {

            const data = await api.get(`/partials/${id}`);

          setPartial(data.partial);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPartial();

    }, []);

    const onSubmit = async (values) => {

        console.log('onSubmit.values:', values);
        try {
            await api.patch(`/partials/${id}`, {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        }
        catch (e) {

            console.log('e:', e);

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
              title={`Editing Partial: ${partial && partial.partialName}`}
              breadcrumb={[...routes, {
                    path: `/layouts/partials/${id}/edit`,
                    text: 'Partial Edit',
                }]}
            />

            <Content className="language-content edit-language">
              {
                partial &&
                <PartialForm data={partial} onSubmit={onSubmit}/>
              }
            </Content>
        </>
    );
};

export default withRouter(EditPartial);
