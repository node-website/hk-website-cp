const isDev = process.env.NODE_ENV === "development";

const apiUrl = {
    development: "http://localhost:30000",
    production: ""
};

export const HTTP = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    PATCH: "PATCH",
    DELETE: "DELETE"
};

export const WEBSITE_ENDPOINT = isDev ? apiUrl.development : apiUrl.production;
export const API_ENDPOINT = `${WEBSITE_ENDPOINT}/api`;

export const APP_FOLDER = `${process.env.PUBLIC_URL}`;
