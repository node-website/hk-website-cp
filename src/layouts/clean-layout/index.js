import React from 'react';

import './style.scss';
import {Layout} from 'antd';
import AppFooter from "../../components/AppFooter";

const CleanLayout = ({children}) => (

    <Layout className="parent-layout">
        {children}
        <AppFooter/>
    </Layout>

);

export default CleanLayout;
