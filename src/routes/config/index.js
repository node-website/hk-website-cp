import React from 'react';
import './style.scss';
import {Card, Layout} from 'antd';
import AppPageHeader from "../../components/AppPageHeader";

const {Content} = Layout;

const routes = [
    {
        path: '/config',
        text: 'Website Config',
    },
];

const Config = () => (
    <>
        <AppPageHeader breadcrumb={routes} title="Website Config"/>

        <Content className="page-content content">
            <Card bordered={false}>

                Website Config Page

            </Card>
        </Content>
    </>
);

export default Config;
