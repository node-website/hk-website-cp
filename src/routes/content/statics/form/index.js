import React, {useEffect, useState} from 'react';
import './style.scss';
import {Button, Card, Col, Form, Icon, Input, Row, Select} from 'antd';
import withForm from "../../../../contexts/withForm";
import api from "../../../../services/api";
import * as _ from "lodash";
import TextEditor from "../../../../components/TextEditor";
import CodeEditor from "../../../../components/CodeEditor";
import ImageGallery from "../../../../components/ImageGallery";
import TagItems from "../../../../components/TagItems";

const ContentForm = ({form, data = {}, onSubmit}) => {

    const [loading, setLoading] = useState(true);
    const [pages, setPages] = useState([]);
    const {getFieldDecorator, validateFields} = form;

    const getPages = async () => {
        try {

            const res = await api.get("/pages?flat=true");

            setPages(res.pages);
            setLoading(false);

        } catch (e) {

            console.log('e:', e);
        }
    };

    useEffect(() => {

        getPages();

    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        validateFields(async (err, fields) => {
            if (err) {
                return;
            }

            onSubmit && onSubmit({
                contentName: fields.contentName,
                title: fields.title,
                subtitle: fields.subtitle,
                images: fields.images || [],
                htmlCode: fields.htmlCode,
                json: fields.json || [],
                connectedPageId: fields.connectedPageId,
                keywords: fields.keywords || [],
            });
        });
    };

    return (
        <Form onSubmit={handleSubmit} className="content-form">
            <Row type="flex" justify="space-between" gutter={12}>
                <Col span={18}>
                    <Card bordered={false}>
                        <Form.Item label="Title">
                            {getFieldDecorator('title', {
                                initialValue: data.title,
                                rules: [{required: true, message: 'Please input title!'}],
                            })(
                                <Input
                                    placeholder="Title"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item label="Subtitle">
                            {getFieldDecorator('subtitle', {
                                initialValue: data.subtitle,
                            })(
                                <Input
                                    placeholder="Subtitle"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item label="Images Album">
                            {getFieldDecorator('images', {
                                initialValue: data.images,
                            })(
                                <ImageGallery/>
                            )}
                        </Form.Item>
                        <Form.Item label="Content Editor">
                            {getFieldDecorator('htmlCode', {
                              initialValue: data.htmlCode || '',
                            })(
                                <TextEditor/>
                            )}
                        </Form.Item>
                        <Form.Item label="JSON Editor">
                            {getFieldDecorator('json', {
                                initialValue: data.json,
                            })(
                                <CodeEditor mode="application/json"/>
                            )}
                        </Form.Item>
                    </Card>
                </Col>
                <Col span={6}>
                    <Card bordered={false}>
                        <Form.Item label="Content Name">
                            {getFieldDecorator('contentName', {
                              initialValue: data.contentName,
                              rules: [{required: true, message: 'Please input content name!'}],
                            })(
                                <Input
                                    placeholder="Content Name"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item label="Connect Other Page">
                            {getFieldDecorator('connectedPageId', {
                                initialValue: data.connectedPageId || '',
                            })(
                                <Select
                                    prefix={<Icon type="user"/>}
                                    placeholder="Page"
                                    allowClear
                                    loading={loading}>
                                    {
                                        _.map(pages, ({_id, title}) => (
                                            <Select.Option key={_id} value={_id}>{title}</Select.Option>
                                        ))
                                    }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="Keywords">
                            {getFieldDecorator('keywords', {
                                initialValue: data.keywords || [],
                            })(
                                <TagItems/>
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="save-button">
                                Save
                            </Button>
                        </Form.Item>
                    </Card>
                </Col>
            </Row>
        </Form>
    );
};

export default withForm(ContentForm);
