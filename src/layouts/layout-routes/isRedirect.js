import Session from "../../services/session";

export default (isPublic) => {

    // private route ve giriş yapmamış ise isRedirect=true olacaktır.
    if (!isPublic) {

        if (!Session.isLogon()) {
            return true;
        }
    }

    return false;
}
