import React, {forwardRef} from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {UploadAdapter} from "./UploadAdapter";
import './style.scss';

const TextEditor = forwardRef(({value, onChange, config, ...otherProps}, ref) => {

    const props = {
        ref,
        className: "content-editor",
        editor: ClassicEditor,
        data: value || '',
        // UploadAdapter,
        config: {
          toolbar: [
            "undo", "redo", "bold", "italic", "blockQuote", "imageTextAlternative", "imageUpload", "heading", "imageStyle:full", "imageStyle:side",
            "indent", "outdent", "link", "numberedList", "bulletedList", "mediaEmbed", "insertTable", "tableColumn", "tableRow", "mergeTableCells"
          ],
            ...config
        },
        onInit: editor => {

            editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
                return new UploadAdapter(loader);
            };
        },
        onChange: (event, editor) => {
            const data = editor.getData();
            onChange && onChange(data);
        },
    };

    return (
        <CKEditor
            {...props}
            {...otherProps}
        />
    );
});

export default TextEditor;
