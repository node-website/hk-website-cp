import React from 'react';
import {Button, Card, Icon, Layout} from 'antd';

import './style.scss';
import {withRouter} from "react-router-dom";

const Error404 = ({history}) => {

    return (
        <Layout.Content className="error404-page">


            <Card bordered={false}>

                404 Kere Aradık Tarafık Hiç Bişey Bulamadık :(

            </Card>

            <Button type="link" block onClick={history.goBack}>
                <Icon type="left"/>
                Back
            </Button>

        </Layout.Content>
    );
};

export default withRouter(Error404);
