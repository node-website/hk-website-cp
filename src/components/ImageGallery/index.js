import React, {forwardRef, useState} from "react";
import _ from "lodash";
import {Icon, Modal, Upload} from 'antd';
import {API_ENDPOINT, WEBSITE_ENDPOINT} from "../../envirmoment";

import './style.scss';

const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
};

const UploadButton = () => (
    <div>
        <Icon type="plus"/>
        <div className="ant-upload-text">Upload</div>
    </div>
);

const imageToUI = (images) => _.map(images, (item, index) => ({
    uid: `${item.createdTime}-${index}`,
    name: item.originalname,
    status: 'done',
    url: `${WEBSITE_ENDPOINT}${item.filename}`,
    ...item
}));

const uiToImage = (fileList) => _.map(fileList, ({response, originalname, createdTime, filename, mimetype, size}) => {
    if (response) {
        return _.result(response, 'uploaded[0]'); // : {...otherFields}
    }

    return {createdTime, originalname, filename, mimetype, size}
});

const ImageGallery = forwardRef(({value, onChange, limit, ...others}, ref) => {

    const [fileList, setFileList] = useState(imageToUI(value));
    const [previewImage, setPreviewImage] = useState();

    const handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        setPreviewImage(file.url || file.preview);
    };

    const handleChange = ({fileList}) => {
        setFileList(fileList);
        onChange(uiToImage(fileList));
    };

    const handleCancel = () => setPreviewImage(undefined);

    return (
        <>
            <Upload
                ref={ref}
                action={`${API_ENDPOINT}/upload`}
                name="files"
                listType="picture-card"
                fileList={fileList}
                onPreview={handlePreview}
                onChange={handleChange}
            >
                {
                    _.isUndefined(limit)
                        ? <UploadButton/>
                        : fileList.length <= limit && <UploadButton/>
                }
            </Upload>
            <Modal visible={!!previewImage} footer={null} onCancel={handleCancel}>
                <img alt="example" style={{width: '100%'}} src={previewImage}/>
            </Modal>
        </>
    );
});

export default ImageGallery;
