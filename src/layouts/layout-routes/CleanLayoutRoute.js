import React from 'react';
import {Route} from 'react-router-dom';

import LoginRedirect from "../../components/LoginRedirect";
import CleanLayout from '../clean-layout';
import isRedirect from "./isRedirect";

const CleanLayoutRoute = ({isPublic, component: Component, ...rest}) => {
    return (
        <Route
            {...rest}
            render={matchProps => {

                if (isRedirect(isPublic)) {
                    return (<LoginRedirect/>);
                }

                return (
                    <CleanLayout>
                        <Component {...matchProps} />
                    </CleanLayout>
                )
            }}
        />
    )
};


export default CleanLayoutRoute;
