import Storage, {STORAGE_USER} from "./storage";

class Session {

    setAuthUser(user) {
        Storage.setItem(STORAGE_USER, user);
    }

    getAuthUser() {
        Storage.getItem(STORAGE_USER);
    }

    isLogon() {
        return !!Storage.getItem(STORAGE_USER);
    }

    clearAuthUser() {
        Storage.removeItem(STORAGE_USER);
    }

}

// Make it singleton.
export default new Session();
