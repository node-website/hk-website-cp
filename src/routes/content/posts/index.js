import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import {Button, Card, Layout, message, Popconfirm, Table, Tag, Typography} from 'antd';
import AppPageHeader from "../../../components/AppPageHeader";
import {Link} from "react-router-dom";
import api from "../../../services/api";

import './style.scss';

const {Text} = Typography;

const routes = [
    {
      path: '/contents/posts',
      text: 'Posts',
    },
];

const Post = () => {

    const [posts, setPosts] = useState([]);
    const [loading, setLoading] = useState(true);

    const getPosts = async () => {
        try {

          const data = await api.get("/posts");

            setPosts(_.map(data.posts, (post) => ({key: post._id, ...post})));
            setLoading(false);

        } catch (e) {

          message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPosts();

    }, []);

    const onDelete = (id) => async () => {
        try {
          await api.delete(`/posts/${id}`);

            message.success("Delete Success");

            getPosts();
        } catch (e) {

          message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="Posts"
                breadcrumb={routes}
            />

            <Layout.Content className="page-content post">
              <Card
                bordered={false}
                title="Posts"
                extra={
                  <>
                    <Link to="/contents/posts/add">
                      <Button type="primary" ghost size="small" icon="plus"/>
                    </Link>
                  </>
                }
              >

                    <Table
                        dataSource={posts}
                        loading={loading}
                        size="small"
                        bordered={true}
                    >
                        <Table.Column
                            title="Title"
                            dataIndex="title"
                            key="title"
                            render={(id, item) => (
                                <>
                                    <Text>{item.title}</Text>
                                    {
                                        item.subtitle &&
                                        <Text type="secondary">{item.subtitle}</Text>
                                    }
                                </>
                            )}
                        />
                        <Table.Column
                            title="Actions"
                            dataIndex="_id"
                            key="_id"
                            width={90}
                            align="center"
                            render={(id) => (
                                <div className="actions">
                                  <Link to={`/contents/posts/${id}/edit`}>
                                        <Button type="primary" ghost size="small" icon="edit"/>
                                    </Link>
                                    <Popconfirm
                                        placement="bottomRight"
                                        title="Delete this post?"
                                        onConfirm={onDelete(id)}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <Button type="danger" ghost size="small" icon="delete"/>
                                    </Popconfirm>
                                </div>
                            )}
                        />
                    </Table>
                </Card>
            </Layout.Content>
        </>
    )
};

export default Post;
