import React, {useEffect, useState} from 'react';
import './style.scss';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import PostForm from "../form";
import api from "../../../../services/api";
import {withRouter} from "react-router-dom";

const routes = [
    {
        path: '/contents/posts',
        text: 'Posts',
    },
];

const EditPost = ({match, history}) => {

    const {id} = match.params;
    const [post, setPost] = useState();

    const getPost = async () => {
        try {

            const data = await api.get(`/posts/${id}`);

            setPost(data.post);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPost();
    }, []);

    const onSubmit = async (values) => {
        try {
            await api.patch(`/posts/${id}`, {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title={`Editing Post: ${post && post.title}`}
                breadcrumb={[...routes, {
                    path: `/contents/posts/${id}/edit`,
                    text: 'Edit Post',
                }]}
            />

            <Layout.Content className="page-content edit-post">
                <PostForm data={post} onSubmit={onSubmit}/>
            </Layout.Content>
        </>
    );
};

export default withRouter(EditPost);
