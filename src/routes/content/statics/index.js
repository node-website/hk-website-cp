import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import {Button, Card, Layout, message, Popconfirm, Table, Tag, Typography} from 'antd';
import AppPageHeader from "../../../components/AppPageHeader";
import {Link} from "react-router-dom";
import api from "../../../services/api";

import './style.scss';

const {Text} = Typography;

const routes = [
    {
      path: '/contents/statics',
      text: 'Static Contents',
    },
];

const Content = () => {

    const [contents, setContents] = useState([]);
    const [loading, setLoading] = useState(true);

    const getContents = async () => {
        try {

          const data = await api.get("/contents");

            setContents(_.map(data.contents, (content) => ({key: content._id, ...content})));
            setLoading(false);

        } catch (e) {

          message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getContents();

    }, []);

    const onDelete = (id) => async () => {
        try {
          await api.delete(`/contents/${id}`);

            message.success("Delete Success");

            getContents();
        } catch (e) {

          message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="Static Contents"
                breadcrumb={routes}
            />

            <Layout.Content className="page-content content">
              <Card
                bordered={false}
                title="Static Contents"
                extra={
                  <>
                    <Link to="/contents/statics/add">
                      <Button type="primary" ghost size="small" icon="plus"/>
                    </Link>
                  </>
                }
              >

                    <Table
                        dataSource={contents}
                        loading={loading}
                        size="small"
                        bordered={true}
                    >
                        <Table.Column
                            title="Title"
                            dataIndex="title"
                            key="title"
                            render={(id, item) => (
                                <>
                                    <Text>{item.title}</Text>
                                    {
                                        item.subtitle &&
                                        <Text type="secondary">{item.subtitle}</Text>
                                    }
                                </>
                            )}
                        />
                        <Table.Column
                            title="Actions"
                            dataIndex="_id"
                            key="_id"
                            width={90}
                            align="center"
                            render={(id) => (
                                <div className="actions">
                                  <Link to={`/contents/statics/${id}/edit`}>
                                        <Button type="primary" ghost size="small" icon="edit"/>
                                    </Link>
                                    <Popconfirm
                                        placement="bottomRight"
                                        title="Delete this content?"
                                        onConfirm={onDelete(id)}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <Button type="danger" ghost size="small" icon="delete"/>
                                    </Popconfirm>
                                </div>
                            )}
                        />
                    </Table>
                </Card>
            </Layout.Content>
        </>
    )
};

export default Content;
