import React from 'react';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import LanguageForm from "../form";

import './style.scss';
import {withRouter} from "react-router-dom";
import api from "../../../../services/api";

const {Content} = Layout;

const routes = [
    {
        path: '/layouts/languages',
        text: 'Languages',
    },
    {
        path: '/layouts/languages/add',
        text: 'New Language',
    },
];

const AddLanguage = ({history}) => {

    const onSubmit = async (values) => {
        try {
            await api.post("/languages", {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="New Language"
                breadcrumb={routes}
            />

            <Content className="page-content add-language">

                <LanguageForm onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(AddLanguage);
