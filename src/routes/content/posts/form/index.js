import React, {useEffect, useState} from 'react';
import './style.scss';
import {Button, Card, Col, Form, Icon, Input, message, Row, Select} from 'antd';
import withForm from "../../../../contexts/withForm";
import api from "../../../../services/api";
import * as _ from "lodash";
import TextEditor from "../../../../components/TextEditor";
import TagItems from "../../../../components/TagItems";

const PostForm = ({form, data = {}, onSubmit}) => {

    const [loading, setLoading] = useState(true);
    const [pages, setPages] = useState([]);
    const [languages, setLanguages] = useState([]);
    const {getFieldDecorator, validateFields} = form;

    const getPages = async () => {
        try {

            const res = await api.get("/pages?flat=true");

            setPages(res.pages);
            setLoading(false);

        } catch (e) {

            console.log('e:', e);
        }
    };

    const getLanguages = async () => {
        try {

            const {languages} = await api.get("/languages");

            setLanguages(languages);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPages();
        getLanguages();

    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        validateFields(async (err, fields) => {
            if (err) {
                return;
            }

            onSubmit && onSubmit({
                langIds: fields.langIds,
                layoutPageId: fields.layoutPageId,
                slug: fields.slug,
                title: fields.title,
                subtitle: fields.subtitle,
                htmlCode: fields.htmlCode,
                keywords: fields.keywords || [],
            });
        });
    };

    return (
        <Form onSubmit={handleSubmit} className="post-form">
            <Row type="flex" justify="space-between" gutter={12}>
                <Col span={18}>
                    <Card bordered={false}>
                        <Form.Item label="Permalink URL">
                            {getFieldDecorator('slug', {
                                initialValue: data.slug,
                            })(
                                <Input
                                    placeholder="Permalink URL"
                                    addonAfter=".html"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item label="Title">
                            {getFieldDecorator('title', {
                                initialValue: data.title,
                                rules: [{required: true, message: 'Please input title!'}],
                            })(
                                <Input
                                    placeholder="Title"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item label="Subtitle">
                            {getFieldDecorator('subtitle', {
                                initialValue: data.subtitle,
                            })(
                                <Input
                                    placeholder="Subtitle"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item label="Post Editor">
                            {getFieldDecorator('htmlCode', {
                                initialValue: data.htmlCode,
                            })(
                                <TextEditor/>
                            )}
                        </Form.Item>
                    </Card>
                </Col>
                <Col span={6}>
                    <Card bordered={false}>
                        <Form.Item label="Page Language">
                            {getFieldDecorator('langIds', {
                                initialValue: data.langIds,
                                rules: [{required: true, message: 'Please input Languages!'}],
                            })(
                                <Select
                                    prefix={<Icon type="user"/>}
                                    mode="multiple"
                                    allowClear
                                    placeholder="Languages">
                                    {
                                        _.map(languages, ({_id, name}) => (
                                            <Select.Option key={_id} value={_id}>{name}</Select.Option>
                                        ))
                                    }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="Layout Page">
                            {getFieldDecorator('layoutPageId', {
                                initialValue: data.layoutPageId,
                            })(
                                <Select
                                    prefix={<Icon type="user"/>}
                                    placeholder="Page"
                                    allowClear
                                    loading={loading}>
                                    {
                                        _.map(pages, ({_id, title}) => (
                                            <Select.Option key={_id} value={_id}>{title}</Select.Option>
                                        ))
                                    }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="Keywords">
                            {getFieldDecorator('keywords', {
                                initialValue: data.keywords || [],
                            })(
                                <TagItems/>
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="save-button">
                                Save
                            </Button>
                        </Form.Item>
                    </Card>
                </Col>
            </Row>
        </Form>
    );
};

export default withForm(PostForm);
