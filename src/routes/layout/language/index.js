import React, {useEffect, useState} from 'react';
import './style.scss';
import {Button, Card, Col, Icon, Layout, message, Popconfirm, Row, Table} from 'antd';
import AppPageHeader from "../../../components/AppPageHeader";
import api from "../../../services/api";
import {Link} from "react-router-dom";

const {Content} = Layout;

const routes = [
  {
    path: '/layouts/language',
    text: 'Language',
  },
];

const Language = () => {

  const [languageLoading, setLanguageLoading] = useState(true);
  const [languages, setLanguages] = useState([]);

  // const [translateLoading, setTranslateLoading] = useState(false);
  // const [translates, setTranslates] = useState([]);

  const getLanguages = async () => {

    try {

      const data = await api.get("/languages");

      setLanguages(data.languages);

    }
    catch (e) {

      message.error(e.error || "Unknown Error");
    }

    setLanguageLoading(false);
  };

  // const getTranslates = (id) => async () => {
  //   setTranslateLoading(true);
  //
  //   try {
  //
  //     const data = await api.get("/translates", {langId: id});
  //
  //     setTranslates(data.translates);
  //
  //   }
  //   catch (e) {
  //
  //       message.error(e.error || "Unknown Error");
  //   }
  //
  //   setTranslateLoading(false);
  // };

  const onDeleteLanguage = (id) => async () => {
    try {

      await api.delete(`/languages/${id}`);

      message.success("Delete Success");

      getLanguages();
    }
    catch (e) {

      message.error(e.error || "Unknown Error");
    }
  };

  useEffect(() => {

    getLanguages();
    // getTranslates();

  }, []);

  return (
    <>
      <AppPageHeader
        breadcrumb={routes}
        title="Language Manager"
        subtitle="Languages & Translates"
      />

      <Content className="page-content page">

        <Row gutter={16}>
          <Col span={10}>
            <Card
              bordered={false}
              title="Languages"
              extra={
                <>
                  <Link to="/layouts/languages/add">
                    <Button type="primary" ghost size="small" icon="plus"/>
                  </Link>
                </>
              }
            >
              <Table
                dataSource={languages}
                loading={languageLoading}
                rowKey="name"
                size="small"
                bordered={true}
              >
                <Table.Column
                  title="Language Text"
                  dataIndex="name"
                  key="name"
                />
                <Table.Column
                  title="Language Code"
                  dataIndex="code"
                  key="code"
                />
                <Table.Column
                  title="Is Default"
                  dataIndex="isDefault"
                  key="isDefault"
                  align="center"
                  render={(isDefault) => (<Icon type={isDefault ? 'check' : 'close'}/>)}
                />
                <Table.Column
                  title="Actions"
                  dataIndex="_id"
                  key="_id"
                  width={120}
                  align="center"
                  render={(id) => (
                    <div className="actions">
                      {/*<Button
                        type="primary"
                        ghost
                        size="small"
                        icon="eye"
                        onClick={getTranslates(id)}
                      />*/}
                      <Link to={`/layouts/languages/${id}/edit`}>
                        <Button type="primary" ghost size="small" icon="edit"/>
                      </Link>
                      <Popconfirm
                        placement="bottomRight"
                        title="Delete this language?"
                        onConfirm={onDeleteLanguage(id)}
                        okText="Yes"
                        cancelText="No"
                      >
                        <Button type="danger" ghost size="small" icon="delete"/>
                      </Popconfirm>
                    </div>
                  )}
                />
              </Table>
            </Card>
          </Col>
          <Col span={14}>
            {/*<Card
              bordered={false}
              title="Translates"
              extra={
                <>
                  <Button type="primary" ghost>
                    <Link to="/layouts/translates/add">New Translate</Link>
                  </Button>
                </>
              }
            >
              {
                JSON.stringify(translates, null, 4)
              }
            </Card>*/}
          </Col>
        </Row>

      </Content>
    </>
  );
};

export default Language;
