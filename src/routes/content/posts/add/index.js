import React from 'react';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import PostForm from "../form";
import api from "../../../../services/api";

import './style.scss';
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
    {
        path: '/contents/posts',
        text: 'Posts',
    },
    {
        path: '/contents/posts/add',
        text: 'New Post',
    },
];

const AddPost = ({history}) => {

    const onSubmit = async (values) => {
        try {
            await api.post("/posts", {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {
            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="New Post"
                breadcrumb={routes}
            />

            <Content className="page-content add-post">

                <PostForm onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(AddPost);
