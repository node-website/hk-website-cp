import React from 'react';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import TranslateForm from "../form";

import './style.scss';
import {withRouter} from "react-router-dom";
import api from "../../../../services/api";

const {Content} = Layout;

const routes = [
    {
        path: '/layouts',
        text: 'Layouts',
    },
    {
        path: '/layouts/languages',
        text: 'Languages',
    },
    {
        path: '/layouts/translates/add',
        text: 'New Translate',
    },
];

const AddTranslate = ({history}) => {

    const onSubmit = async (values) => {
        try {
            await api.post("/translates", {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="New Translate"
                breadcrumb={routes}
            />

            <Content className="page-content add-language">

                <TranslateForm onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(AddTranslate);
