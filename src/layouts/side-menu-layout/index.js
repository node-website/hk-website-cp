import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import * as _ from "lodash";
import {Icon, Layout, Menu} from 'antd';

import menus from "./menus";

import './style.scss';

const {Sider} = Layout;

const SideMenuLayout = ({children, location}) => {

    const {pathname} = location;

    return (
        <Layout className="parent-layout side-menu-layout">
            <Sider theme="light" width={220}>
                <div className="logo"/>
                <Menu
                    theme="light"
                    mode="inline"
                    defaultSelectedKeys={[pathname]}
                    selectedKeys={[pathname]}
                >

                    {_.map(menus, (menu) => (menu.children && menu.children.length)
                        ? (
                            <Menu.SubMenu
                                key={menu.path}
                                title={
                                    <span className="submenu-title-wrapper">
                                            <Icon type={menu.icon}/>
                                        {menu.text}
                                        </span>
                                }
                            >
                                {
                                    _.map(menu.children, (childMenu) => (
                                        <Menu.Item key={menu.path + childMenu.path}>
                                            <Link to={menu.path + childMenu.path}>
                                                <Icon type={childMenu.icon}/>
                                                <span className="nav-text">{childMenu.text}</span>
                                            </Link>
                                        </Menu.Item>
                                    ))
                                }
                            </Menu.SubMenu>
                        )
                        : (
                            <Menu.Item key={menu.path}>
                                <Link to={menu.path}>
                                    <Icon type={menu.icon}/>
                                    <span className="nav-text">{menu.text}</span>
                                </Link>
                            </Menu.Item>
                        )
                    )}

                </Menu>
            </Sider>
            <Layout className="content-container">
                {children}
            </Layout>
        </Layout>

    );
};

export default withRouter(SideMenuLayout);
