import React, {useEffect, useState} from 'react';
import './style.scss';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import ContentForm from "../form";
import api from "../../../../services/api";
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
    {
        path: '/contents/statics',
        text: 'Static Contents',
    },
];

const EditContent = ({match, history}) => {

    const {id} = match.params;
    const [content, setContent] = useState();

    const getContent = async () => {
        try {

            const data = await api.get(`/contents/${id}`);

            setContent(data.content);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getContent();
    }, []);

    const onSubmit = async (values) => {
        try {
            await api.patch(`/contents/${id}`, {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title={`Editing Content: ${content && content.title}`}
                breadcrumb={[...routes, {
                    path: `/contents/statics/${id}/edit`,
                    text: 'Edit Content',
                }]}
            />

            <Content className="page-content edit-content">
                <ContentForm data={content} onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(EditContent);
