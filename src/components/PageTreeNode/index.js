import React from "react";
import * as _ from "lodash";
import {Tree} from 'antd';

import './style.scss';

const renderTreeNodes = (data, parentKey = "") => _.map(data, (item, index) => {

    const pKey = parentKey ? `${parentKey}.children` : "";
    const key = `${pKey}[${index}]`;

    if (item.children) {

        return (
            <Tree.TreeNode title={item.title} key={key} dataRef={item}>
                {renderTreeNodes(item.children, key)}
            </Tree.TreeNode>
        );
    }

    return <Tree.TreeNode key={key} title={item.title}/>;
});

export default renderTreeNodes;
