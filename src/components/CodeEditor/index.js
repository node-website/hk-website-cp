import React, {forwardRef} from "react";
import PropTypes from 'prop-types';
import {UnControlled as CodeMirror} from 'react-codemirror2'

import 'codemirror/mode/htmlmixed/htmlmixed';
import 'codemirror/mode/css/css';
import 'codemirror/mode/htmlembedded/htmlembedded';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/material.css';

import './style.scss';

const CodeEditor = forwardRef(({value, onChange, mode, options, ...otherProps}, ref) => {

    const props = {
        ref,
        className: "code-editor",
        autoCursor: false,
        value,
        onChange: (codemirror, event, text) => {
          onChange && onChange(text);
        },
        options: {
            lineNumbers: true,
            theme: 'material',
            mode,
            ...options
        },
        autoFocus: true
    };

    return (
        <CodeMirror
            {...props}
            {...otherProps}
        />
    );
});

CodeEditor.propTypes = {
  options: PropTypes.object,
  mode: PropTypes.oneOf([
    "htmlmixed",
    "text/css",
    "application/x-ejs",
    "application/json"
  ])
};

CodeEditor.defaultProps = {
  mode: "application/x-ejs",
  onChange: () => {
  }
};

// export default withRouter(AppPageHeader);
export default CodeEditor;
