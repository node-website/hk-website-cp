import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import {Redirect, Route, Switch} from "react-router-dom";
import * as serviceWorker from './serviceWorker';

import './styles/style.scss';
import {RootProvider} from "./contexts/RootContext";
import {
  ConfigView,
  ContentAddView,
  ContentEditView,
  ContentView,
  Error404View,
  HomeView,
  LanguageAddView,
  LanguageEditView,
  LanguageView,
  LoginView,
  PageAddView,
  PageEditView,
  PageView,
  PartialAddView,
  PartialEditView,
  PartialView,
  PostAddView,
  PostEditView,
  PostView,
  ProfileView,
  StyleAddView,
  StyleEditView,
  StyleView,
  TranslateAddView,
  TranslateEditView,
  UserView
} from "./routes";
import {CleanLayoutRoute, SideMenuLayoutRoute} from "./layouts/layout-routes";
import api from "./services/api";

export const App = () => {

  useEffect(() => {
    api.post('/auth').catch(console.log)
  }, []);

  return (
    <RootProvider>
      <Switch>
        <CleanLayoutRoute exact path="/login" component={LoginView} isPublic/>

        <SideMenuLayoutRoute exact path="/" component={HomeView}/>

        <SideMenuLayoutRoute exact path="/layouts/languages" component={LanguageView}/>
        <SideMenuLayoutRoute exact path="/layouts/languages/add" component={LanguageAddView}/>
        <SideMenuLayoutRoute exact path="/layouts/languages/:id/edit" component={LanguageEditView}/>
        <SideMenuLayoutRoute exact path="/layouts/translates/add" component={TranslateAddView}/>
        <SideMenuLayoutRoute exact path="/layouts/translates/:id/edit" component={TranslateEditView}/>

        <SideMenuLayoutRoute exact path="/layouts/pages" component={PageView}/>
        <SideMenuLayoutRoute exact path="/layouts/pages/add" component={PageAddView}/>
        <SideMenuLayoutRoute exact path="/layouts/pages/:id/edit" component={PageEditView}/>

        <SideMenuLayoutRoute exact path="/layouts/partials" component={PartialView}/>
        <SideMenuLayoutRoute exact path="/layouts/partials/add" component={PartialAddView}/>
        <SideMenuLayoutRoute exact path="/layouts/partials/:id/edit" component={PartialEditView}/>

        <SideMenuLayoutRoute exact path="/layouts/styles" component={StyleView}/>
        <SideMenuLayoutRoute exact path="/layouts/styles/add" component={StyleAddView}/>
        <SideMenuLayoutRoute exact path="/layouts/styles/:id/edit" component={StyleEditView}/>

        <SideMenuLayoutRoute exact path="/contents/statics" component={ContentView}/>
        <SideMenuLayoutRoute exact path="/contents/statics/add" component={ContentAddView}/>
        <SideMenuLayoutRoute exact path="/contents/statics/:id/edit" component={ContentEditView}/>

        <SideMenuLayoutRoute exact path="/contents/posts" component={PostView}/>
        <SideMenuLayoutRoute exact path="/contents/posts/add" component={PostAddView}/>
        <SideMenuLayoutRoute exact path="/contents/posts/:id/edit" component={PostEditView}/>

        <SideMenuLayoutRoute exact path="/users" component={UserView}/>
        <SideMenuLayoutRoute exact path="/web-configs" component={ConfigView}/>
        <SideMenuLayoutRoute exact path="/profile" component={ProfileView}/>

        <CleanLayoutRoute exact path="/404" component={Error404View}/>

        <Route exact path="**">
          <Redirect to="/404"/>
        </Route>
      </Switch>
    </RootProvider>
  );
};

ReactDOM.render(<App/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
