import React from 'react';
import './style.scss';
import {Layout} from 'antd';

const {Content} = Layout;

const Home = () => (
    <>
        <Content className="page-content home"></Content>
    </>
);

export default Home;
