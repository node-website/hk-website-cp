const tryParse = (item) => {
    try {
        return JSON.parse(item);
    }
    catch (e) {
        return null;
    }
};

class Storage {

    localStorage = window.localStorage;

    getItem(key) {
        return tryParse(this.localStorage.getItem(key));
    }

    setItem(key, value) {
        this.localStorage.setItem(key, JSON.stringify(value));
    }

    removeItem(key) {
        this.localStorage.removeItem(key);
    }
}

// Make it singleton.
export default new Storage();

export const STORAGE_USER = "cp-user";
