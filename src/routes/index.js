// import HomePage from "./home";
// import UserView from "./user";
// import LoginPage from "./login";
// import AboutPage from "./about";
// import Error404View from "./error404";
//
// const lazyImporter = (url, timeout = 300) => lazy(() => {
//     return new Promise(resolve => {
//         setTimeout(() => resolve(import(url)), timeout);
//     });
// });

import {lazy} from "react";

const LoginView = lazy(() => import("./login"));

const HomeView = lazy(() => import("./home"));

const LanguageView = lazy(() => import("./layout/language"));
const LanguageAddView = lazy(() => import("./layout/language/add"));
const LanguageEditView = lazy(() => import("./layout/language/edit"));
const TranslateAddView = lazy(() => import("./layout/translate/add"));
const TranslateEditView = lazy(() => import("./layout/translate/edit"));

const PageView = lazy(() => import("./layout/page"));
const PageAddView = lazy(() => import("./layout/page/add"));
const PageEditView = lazy(() => import("./layout/page/edit"));

const PartialView = lazy(() => import("./layout/partial"));
const PartialAddView = lazy(() => import("./layout/partial/add"));
const PartialEditView = lazy(() => import("./layout/partial/edit"));

const StyleView = lazy(() => import("./layout/style"));
const StyleAddView = lazy(() => import("./layout/style/add"));
const StyleEditView = lazy(() => import("./layout/style/edit"));

const ContentView = lazy(() => import("./content/statics"));
const ContentAddView = lazy(() => import("./content/statics/add"));
const ContentEditView = lazy(() => import("./content/statics/edit"));

const PostView = lazy(() => import("./content/posts"));
const PostAddView = lazy(() => import("./content/posts/add"));
const PostEditView = lazy(() => import("./content/posts/edit"));

const UserView = lazy(() => import("./user"));
const ConfigView = lazy(() => import("./config"));
const ProfileView = lazy(() => import("./profile"));

const Error404View = lazy(() => import("./error404"));

export {
    PageView,
    PageAddView,
    PageEditView,

    PartialView,
    PartialAddView,
    PartialEditView,

    StyleView,
    StyleAddView,
    StyleEditView,

    LanguageView,
    LanguageAddView,
    LanguageEditView,
    TranslateAddView,
    TranslateEditView,

    ContentView,
    ContentAddView,
    ContentEditView,

    PostView,
    PostAddView,
    PostEditView,

    ConfigView,
    Error404View,
    HomeView,
    LoginView,
    ProfileView,
    UserView
};
