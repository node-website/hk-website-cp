import React from 'react';
import {Button, Card, Form, Icon, Input, Layout} from 'antd';

import './style.scss';
import api from "../../services/api";
import session from "../../services/session";
import withForm from "../../contexts/withForm";

const Login = ({form, history}) => {

    const {getFieldDecorator, validateFields} = form;

    const handleSubmit = (e) => {
        e.preventDefault();

        validateFields(async (err, values) => {
            if (err) {
                return;
            }

            const data = await api.post("/login", {...values});

            if (data) {
                session.setAuthUser(data.user);

                history.replace("/");
            }
        });
    };

    return (
        <Layout.Content className="page-content login">


            <Card title="Giriş Yapın" bordered={false}>
                <Form onSubmit={handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{required: true, message: 'Please input your username!'}],
                        })(
                            <Input
                                prefix={<Icon type="user"/>}
                                placeholder="Username"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Please input your Password!'}],
                        })(
                            <Input
                                prefix={<Icon type="lock"/>}
                                type="password"
                                placeholder="Password"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
            </Card>

        </Layout.Content>
    );
};

export default withForm(Login);
