import React from 'react';
import {Route} from 'react-router-dom';

import LoginRedirect from "../../components/LoginRedirect";
import MainLayout from '../main-layout';
import isRedirect from "./isRedirect";

const MainLayoutRoute = ({isPublic, component: Component, ...rest}) => {
    return (
        <Route
            {...rest}
            render={matchProps => {

                if (isRedirect(isPublic)) {
                    return (<LoginRedirect/>);
                }

                return (
                    <MainLayout>
                        <Component {...matchProps} />
                    </MainLayout>
                )
            }}
        />
    )
};


export default MainLayoutRoute;
