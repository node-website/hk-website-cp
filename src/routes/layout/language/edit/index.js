import React, {useEffect, useState} from 'react';
import './style.scss';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import LanguageForm from "../form";
import api from "../../../../services/api";
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
  {
    path: '/layouts/languages',
    text: 'Languages',
  },
];

const EditLanguage = ({match, history}) => {

    const {id} = match.params;
    const [language, setLanguage] = useState();

    const getLanguage = async () => {
        try {

            const data = await api.get(`/languages/${id}`);

            setLanguage(data.language);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getLanguage();

    }, []);

    const onSubmit = async (values) => {

        console.log('onSubmit.values:', values);
        try {
            await api.patch(`/languages/${id}`, {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        }
        catch (e) {

            console.log('e:', e);

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title={`Editing Language: ${language && language.title}`}
                breadcrumb={[...routes, {
                    path: `/language/${id}/edit`,
                    text: 'Language Edit',
                }]}
            />

            <Content className="language-content edit-language">
                <LanguageForm data={language} onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(EditLanguage);
