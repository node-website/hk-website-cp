import React from 'react';
import './style.scss';
import {Card, Layout} from 'antd';
import AppPageHeader from "../../components/AppPageHeader";

const {Content} = Layout;

const routes = [
    {
        path: '/user',
        text: 'User',
    },
];

const User = () => (
    <>
        <AppPageHeader breadcrumb={routes} title="User "/>

        <Content className="page-content content">
            <Card bordered={false}>

                User Page

            </Card>
        </Content>
    </>
);

export default User;
