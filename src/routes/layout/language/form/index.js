import React from 'react';
import './style.scss';
import {Button, Card, Form, Icon, Input, Select, Switch} from 'antd';
import withForm from "../../../../contexts/withForm";
import * as _ from "lodash";

const langDirections = [
  {
    direction: 'ltr',
    text: 'Left to Right'
  },
  {
    direction: 'rtl',
    text: 'Right to Left'
  },
];

const LanguageForm = ({form, data = {}, onSubmit}) => {

    const {getFieldDecorator, validateFields} = form;

    const handleSubmit = (e) => {
        e.preventDefault();

        validateFields(async (err, fields) => {
            if (err) {
                return;
            }

            onSubmit && onSubmit({
              code: fields.code,
              name: fields.name,
              isDefault: fields.isDefault,
              direction: fields.direction,
            });
        });
    };

    return (
        <Form onSubmit={handleSubmit} className="page-form">
          <Card bordered={false}>
            <Form.Item label="Language Code">
              {getFieldDecorator('code', {
                initialValue: data.code,
                rules: [{required: true, message: 'Please input Language Code!'}],
              })(
                <Input
                  placeholder="Language Code"
                />
              )}
            </Form.Item>
            <Form.Item label="Language Name">
              {getFieldDecorator('name', {
                initialValue: data.name,
                rules: [{required: true, message: 'Please input Language Name!'}],
              })(
                <Input
                  placeholder="Language Code"
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator(`isDefault`, {
                initialValue: !!data.isDefault,
                valuePropName: 'checked'
              })(
                <Switch
                  unCheckedChildren="Is not default"
                  checkedChildren="Is default"
                />
              )}
            </Form.Item>
            <Form.Item label="Language Direction">
              {getFieldDecorator('direction', {
                initialValue: data.direction,
              })(
                <Select
                  prefix={<Icon type="user"/>}
                  placeholder="Direction">
                  {
                    _.map(langDirections, ({direction, text}) => (
                      <Select.Option key={direction} value={direction}>{text}</Select.Option>
                    ))
                  }
                </Select>
              )}
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" className="save-button">
                Save
              </Button>
            </Form.Item>
          </Card>
        </Form>
    );
};

export default withForm(LanguageForm);
