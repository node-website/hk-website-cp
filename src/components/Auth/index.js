import React, {useEffect, useState} from "react";
import {Redirect, withRouter} from "react-router-dom";
import session from "../../services/session";
import api from "../../services/api";

const Auth = withRouter(({location}) => {

    const [isRedirect, setRedirect] = useState(false);

    const apiCall = async () => {
        try {

            await api.post('/auth');

        } catch (error) {


            if (error.data && error.data.status === 401) {

                session.clearAuthUser();

                setRedirect(true);
            }
        }
    };

    useEffect(() => {
        apiCall();
    }, []);

    if (isRedirect && location.pathname !== '/login') {
        return (<Redirect to={{pathname: '/login', state: {from: location}}}/>);
    }

    return null;
});

export default Auth;
