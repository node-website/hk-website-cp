import * as _ from "lodash";
import api from "../../services/api";

export class UploadAdapter {

    loader = null;

    constructor(loader) {
        // CKEditor 5's FileLoader instance.
        this.loader = loader;
    }

    // Starts the upload process.
    async upload() {
        try {
            const file = await this.loader.file;

            const formData = new FormData();
            formData.append('files', file);

            const data = await api.post(
                '/upload',
                formData,
                {},
                {
                    isUpload: true,
                    onUploadProgress: (evt) => {

                        if (evt.lengthComputable) {
                            this.loader.uploadTotal = evt.total;
                            this.loader.uploaded = evt.loaded;
                        }
                    }
                }
            );

            return {
                default: _.result(data, 'uploaded[0].filename')
            };

        } catch (e) {

            return {
                "uploaded": 0,
                "error": {
                    "message": "The file is too big."
                }
            };
        }
    }

    // Aborts the upload process.
    abort() {
        console.log("loader:abort");
    }
}
