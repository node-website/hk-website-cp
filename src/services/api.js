import axios from "axios";
import {API_ENDPOINT, HTTP} from "../envirmoment";
import session from "./session";
import {selfRedirect} from "./self-redirect";

const axiosRequest = axios.create({
    withCredentials: true,
});

axiosRequest.interceptors.response.use((res) => res, (error) => {
    // Do something with response error
    let {response} = error;
    // response?.status === 401 es7 way.
    if (response && response.status === 401) {
        // Do logout;
        // Redirect to login;
        return Promise.reject(error.response);
    } else if (error.response) {
        // Do something.
        console.log('some API_CLIENT error');
        return Promise.reject(error.response && error.response.data);
    }

    return Promise.reject({
        error: error.message,
        status: response.status
    });
});

const request = async (method, service, payload = {}, headers = {}, {isUpload, options} = {}) => {
    try {

        const {data} = await axiosRequest({
            url: `${API_ENDPOINT}${service}`,
            data: payload,
            method,
            headers: {
                'Content-Type': !!isUpload ? 'multipart/form-data' : 'application/json',
                ...headers,
            },
            ...options
        });

        return data;

    } catch (error) {

        console.log("error: ", error);


        if (error.data && error.data.status === 401) {

            session.clearAuthUser();

            selfRedirect('/login');

        } else {

            throw error;
        }
    }

};

class Api {

    async get(service, payload = {}, headers = {}, options = {}) {
        return request(HTTP.GET, service, payload, headers, options);
    }

    async post(service, payload = {}, headers = {}, options = {}) {
        return request(HTTP.POST, service, payload, headers, options)
    }

    // async put(service, payload = {}, headers = {}, options = {}) {
    //     return request(HTTP.PUT, service, payload, headers, options);
    // }

    async patch(service, payload = {}, headers = {}, options = {}) {
        return request(HTTP.PATCH, service, payload, headers, options);
    }

    async delete(service, payload = {}, headers = {}, options = {}) {
        return request(HTTP.DELETE, service, payload, headers, options);
    }

}

// Make it singleton.
export default new Api();
