import React, {useMemo, useState} from "react";
import session from "../services/session";
import {Redirect, withRouter} from "react-router-dom";

export const AuthContext = React.createContext({
    user: null,
    setUser: () => {
    }
});

export const AuthProvider = withRouter(({children, location}) => {

    const isLogon = session.isLogon();
    const initialUser = session.getAuthUser();
    const [user, setUser] = useState(initialUser);

    // Memoized auth context
    const userMemo = useMemo(() => ({
        user,
        setUser,
        setAuth(data) {
            setUser(data);
            Storage.setAuth(data);
        }
    }), [user]);

    return (
        <AuthContext.Provider value={userMemo}>
            {
                location.pathname !== '/login' && !isLogon &&
                <Redirect to={{pathname: '/login', state: {from: location}}}/>
            }
            {children}
        </AuthContext.Provider>
    );
});
