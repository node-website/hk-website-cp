import React from 'react';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import ContentForm from "../form";
import api from "../../../../services/api";

import './style.scss';
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
    {
        path: '/contents/statics',
        text: 'Static Contents',
    },
    {
        path: '/contents/statics/add',
        text: 'New Static Content',
    },
];

const AddContent = ({history}) => {

    const onSubmit = async (values) => {
        try {
            await api.post("/contents", {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {
            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="New Static Content"
                breadcrumb={routes}
            />

            <Content className="page-content add-content">

                <ContentForm onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(AddContent);
