import React, {useEffect, useState} from 'react';
import './style.scss';
import {Button, Card, Icon, Layout, message, Popconfirm, Tree} from 'antd';
import AppPageHeader from "../../../components/AppPageHeader";
import api from "../../../services/api";
import * as _ from "lodash";
import renderTreeNodes from "../../../components/PageTreeNode";
import {Link} from "react-router-dom";

const {Content} = Layout;

const routes = [
    {
        path: '/layouts/pages',
        text: 'Pages',
    },
];

const Page = () => {

    const [selected, setSelected] = useState(null);
    const [pages, setPages] = useState([]);

    const getPages = async () => {
        try {

            const data = await api.get("/pages");

            setPages(data.pages);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    const onDeletePage = (id) => async () => {
        try {
            await api.delete(`/pages/${id}`);

            message.success("Delete Success");

            setSelected(null);

            getPages();
        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPages();

    }, []);

    const onSelect = (selected) => {

        if (!selected.length) {
            setSelected(null);

            return;
        }

        const selectedPage = _.result(pages, selected[0]);

        setSelected(selectedPage);
    };

    return (
        <>
            <AppPageHeader
                breadcrumb={routes}
                title="Layouts"
                subtitle="Page & Menu & Partials Editors"
            />

            <Content className="page-content page">

                <Card
                    bordered={false}
                    title="Page & Menu"
                    extra={
                        <>
                            <Link to="/layouts/pages/add">
                                <Button type="primary" ghost size="small" icon="plus"/>
                            </Link>
                            {
                                selected &&
                                <>
                                    <Link to={`/layouts/pages/${selected._id}/edit`}>
                                        <Button type="primary" ghost size="small" icon="edit"/>
                                    </Link>
                                    <Popconfirm
                                        placement="bottomRight"
                                        title="Delete this page?"
                                        onConfirm={onDeletePage(selected._id)}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <Button type="danger" ghost size="small" icon="delete"/>
                                    </Popconfirm>
                                </>
                            }
                        </>
                    }
                >
                    <Tree
                        showLine
                        showIcon
                        switcherIcon={<Icon type="down"/>}
                        onSelect={onSelect}
                        defaultExpandAll={true}
                    >
                        {renderTreeNodes(pages)}
                    </Tree>
                </Card>

            </Content>
        </>
    );
};

export default Page;
