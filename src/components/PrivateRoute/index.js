import React, {useContext} from "react";
import {Redirect, Route, withRouter} from "react-router-dom";
import PropTypes from "prop-types";
// Local Imports
import AuthContext from "../../contexts/AuthContext";

const PrivateRoute = ({component: Component, location, ...rest}) => {

    const {admin} = useContext(AuthContext);
    const {pathname, search} = location;

    if (!admin) {
        return (
            <>
                <Redirect to={{pathname: "/account/login", search: `?redirect_uri=${encodeURIComponent(`${pathname}${search}`)}`}}/>
            </>
        );
    }

    return (
        <Route
            {...rest}
            render={(props) => (
                <Component {...props}/>
            )}
        />
    );
};

PrivateRoute.propTypes = {
    component: PropTypes.func.isRequired,

    // React router
    location: PropTypes.shape()
};

export default withRouter(PrivateRoute);
