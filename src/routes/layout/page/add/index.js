import React from 'react';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import PageForm from "../form";

import './style.scss';
import {withRouter} from "react-router-dom";
import api from "../../../../services/api";

const {Content} = Layout;

const routes = [
    {
        path: '/layouts/pages',
        text: 'Pages',
    },
    {
        path: '/layouts/pages/add',
        text: 'New Page',
    },
];

const AddPage = ({history}) => {

    const onSubmit = async (values) => {
        try {
            await api.post("/pages", {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="New Page"
                breadcrumb={routes}
            />

            <Content className="page-content add-content">

                <PageForm onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(AddPage);
