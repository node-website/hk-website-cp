import React from 'react';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import StyleForm from "../form";

import './style.scss';
import {withRouter} from "react-router-dom";
import api from "../../../../services/api";

const {Content} = Layout;

const routes = [
  {
    path: '/layouts/styles',
    text: 'Styles',
  },
  {
    path: '/layouts/styles/add',
    text: 'New Style',
  },
];

const AddStyle = ({history}) => {

  const onSubmit = async (values) => {
    try {
      await api.post("/styles", {...values});

      message.success("Save Success");

      setTimeout(history.goBack, 3000);
    }
    catch (e) {

      message.error(e.error || "Unknown Error");
    }
  };

  return (
    <>
      <AppPageHeader
        title="New Style"
        breadcrumb={routes}
      />

      <Content className="page-content add-language">

        <StyleForm onSubmit={onSubmit}/>
      </Content>
    </>
  );
};

export default withRouter(AddStyle);
