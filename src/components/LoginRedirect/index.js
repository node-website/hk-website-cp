import React from "react";
import {Redirect, withRouter} from "react-router-dom";

const LoginRedirect = ({location}) => {

    return (<Redirect to={{pathname: "/login", state: {from: location}}}/>);
};

export default withRouter(LoginRedirect);
