export default [
    {path: "/", text: "Home", icon: "home"},
    {
        path: "/layouts",
        text: "Layouts",
        icon: "layout",
        children: [
            {path: "/languages", text: "Language Manager", icon: "transaction"},
            {path: "/partials", text: "Partials", icon: "deployment-unit"},
            {path: "/styles", text: "Styles", icon: "container"}, // codemirror sayfası olacak.
            {path: "/pages", text: "Pages", icon: "apartment"},
        ]
    },
    {
        path: "/contents",
        text: "Contents",
        icon: "container",
        children: [
            {path: "/statics", text: "Static Content", icon: "container"},
            {path: "/posts", text: "Posts", icon: "apartment"},
        ]
    },
    {
        path: "/data",
        text: "Data",
        icon: "container",
        children: [
            {path: "/users", text: "Users", icon: "user"},
            {path: "/forms", text: "Forms", icon: "apartment"},
        ]
    },
    {path: "/web-configs", text: "Website Config", icon: "control"},
    {path: "/profile", text: "Profile", icon: "profile"},
];
