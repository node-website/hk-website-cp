import React, {useState} from 'react';
import './style.scss';
import {Button, Card, Layout} from 'antd';
import AppPageHeader from "../../components/AppPageHeader";
import api from "../../services/api";
import {selfRedirect} from "../../services/self-redirect";

const {Content} = Layout;

const routes = [
  {
    path: '/profile',
    text: 'Profile',
  },
];

const Profile = () => {

  const [loading, setLoading] = useState(false);

  const logout = async () => {
    setLoading(true);

    try {

      await api.post("/logout");

      selfRedirect("/login")

    }
    catch (e) {

      console.log('e:', e);
    }

    setLoading(false);

  };

  return (
    <>
      <AppPageHeader
        breadcrumb={routes}
        title="Profile"
        buttons={
          <>
            <Button type="danger" ghost loading={loading} onClick={logout}>
              {!loading && 'Logout'}
            </Button>
          </>
        }
      />

      <Content className="page-content content">
        <Card bordered={false}>

          şifre ve rol değiştirme işlemleri buraya hazırlanacak.

        </Card>
      </Content>
    </>
  )
};

export default Profile;
