export default [
    {path: "/", text: "Home", icon: "home"},
    {path: "/user", text: "Users"},
    {path: "/login", text: "Logout"},
];
