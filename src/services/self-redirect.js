import path from 'path';
import {APP_FOLDER} from "../envirmoment";

export const selfRedirect = (pathname) => {

    const pn = path.join(APP_FOLDER, pathname);
    if (window.location.pathname !== pn) {
        window.location.href = pn;
    }

};

