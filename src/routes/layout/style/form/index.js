import React from 'react';
import {Button, Card, Col, Form, Input, Row} from 'antd';
import withForm from "../../../../contexts/withForm";
import CodeEditor from "../../../../components/CodeEditor";

import './style.scss';

const StyleForm = ({form, data = {}, onSubmit}) => {

  const {getFieldDecorator, validateFields} = form;

  const handleSubmit = (e) => {
    e.preventDefault();

    validateFields(async (err, fields) => {
      if (err) {
        return;
      }

      onSubmit && onSubmit({
        code: fields.code,
        styleName: fields.styleName,
      });
    });
  };

  return (
    <Form onSubmit={handleSubmit} className="style-form">
      <Row type="flex" justify="space-between" gutter={12}>
        <Col span={18}>
          <Card bordered={false}>

            <Form.Item label="Style Code">
              {getFieldDecorator('code', {
                initialValue: data.code,
              })(
                <CodeEditor mode="text/css"/>
              )}
            </Form.Item>

          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false}>
            <Form.Item label="Style Name">
              {getFieldDecorator('styleName', {
                initialValue: data.styleName,
                rules: [{required: true, message: 'Please input Style Code!'}],
              })(
                <Input
                  placeholder="Style Name"
                />
              )}
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" className="save-button">
                Save
              </Button>
            </Form.Item>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};

export default withForm(StyleForm);
