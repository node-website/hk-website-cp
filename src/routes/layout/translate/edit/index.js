import React, {useEffect, useState} from 'react';
import './style.scss';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import TranslateForm from "../form";
import api from "../../../../services/api";
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
  {
    path: '/layouts',
    text: 'Layouts',
  },
  {
    path: '/layouts/languages',
    text: 'Languages',
  },
];

const EditTranslate = ({match, history}) => {

    const {id} = match.params;
    const [translate, setTranslates] = useState();

    const getTranslate = async () => {
        try {

            const data = await api.get(`/translates/${id}`);

          setTranslates(data.translates);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getTranslate();

    }, []);

    const onSubmit = async (values) => {

        console.log('onSubmit.values:', values);
        try {
            await api.patch(`/translates/${id}`, {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        }
        catch (e) {

            console.log('e:', e);

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title={`Editing Translate: ${translate && translate.value}`}
                breadcrumb={[...routes, {
                    path: `/layouts/translates/${id}/edit`,
                    text: 'Translate Edit',
                }]}
            />

            <Content className="language-content edit-language">
                <TranslateForm data={translate} onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(EditTranslate);
