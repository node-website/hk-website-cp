import React, {useEffect, useState} from 'react';
import './style.scss';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import PageForm from "../form";
import api from "../../../../services/api";
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
  {
    path: '/layouts/pages',
    text: 'Pages',
  },
];

const EditPage = ({match, history}) => {

    const {id} = match.params;
    const [page, setPage] = useState();

    const getPage = async () => {
        try {

            const data = await api.get(`/pages/${id}`);

            setPage(data.page);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPage();

    }, []);

    const onSubmit = async (values) => {

        try {
            await api.patch(`/pages/${id}`, {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        }
        catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title={`Editing Page: ${page && page.title}`}
                breadcrumb={[...routes, {
                    path: `/layouts/pages/${id}/edit`,
                    text: 'Page Edit',
                }]}
            />

            <Content className="page-content edit-page">
              {
                  page&&
                  <PageForm data={page} onSubmit={onSubmit}/>
              }
            </Content>
        </>
    );
};

export default withRouter(EditPage);
