import React, {useEffect, useState} from 'react';
import './style.scss';
import {Button, Card, Col, Form, Icon, Input, message, Row, Select, Switch} from 'antd';
import withForm from "../../../../contexts/withForm";
import api from "../../../../services/api";
import * as _ from "lodash";
import CodeEditor from "../../../../components/CodeEditor";
import TagItems from "../../../../components/TagItems";

const PageForm = ({form, data = {}, onSubmit}) => {

    const [pages, setPages] = useState([]);
    const [contents, setContents] = useState([]);
    const [languages, setLanguages] = useState([]);
    const {getFieldDecorator, getFieldValue, validateFields} = form;

    const getPages = async () => {
        try {

            const {pages} = await api.get("/pages?flat=true");

            setPages(pages);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    const getContents = async () => {
        try {

            const {contents} = await api.get("/contents");

            setContents(contents);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    const getLanguages = async () => {
        try {

            const {languages} = await api.get("/languages");

            setLanguages(languages);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getPages();
        getContents();
        getLanguages();

    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        validateFields(async (err, fields) => {
            if (err) {
                return;
            }

            onSubmit && onSubmit({
                langIds: fields.langIds,
                slug: fields.slug,
                title: fields.title,
                order: parseInt(fields.order),
                shownMenu: !!fields.shownMenu,
                parentId: fields.parentId,
                keywords: fields.keywords,
                contentIds: fields.contentIds,
                postLayoutIds: fields.postLayoutIds,
                code: fields.code,
            });
        });
    };

    const cardTitle = getFieldValue('title') || data.title;

    return (
        <Form onSubmit={handleSubmit} className="page-form">
            <Row type="flex" justify="space-between" gutter={12}>
                <Col span={18}>
                    <Card bordered={false}>
                        <Form.Item label="Permalink URL">
                            {getFieldDecorator('slug', {
                                initialValue: data.slug,
                                rules: [{required: true, message: 'Please input permalink url!'}],
                            })(
                                <Input
                                    placeholder="Permalink URL"
                                    addonAfter=".html"
                                />
                            )}
                        </Form.Item>
                        <Form.Item label="Title">
                            {getFieldDecorator('title', {
                                initialValue: data.title,
                                rules: [{required: true, message: 'Please input title!'}],
                            })(
                                <Input
                                    placeholder="Title"
                                />
                            )}
                        </Form.Item>
                        <Form.Item label={cardTitle ? `${cardTitle}.ejs` : "Input View Name!"}>
                            {getFieldDecorator('code', {
                                initialValue: data.code,
                                rules: [{required: true, message: 'Please input code!'}],
                            })(
                                <CodeEditor options={{height: '80%'}}/>
                            )}
                        </Form.Item>
                    </Card>
                </Col>
                <Col span={6}>
                    <Card bordered={false}>
                        <Form.Item label="Page Language">
                            {getFieldDecorator('langIds', {
                                initialValue: data.langIds,
                                rules: [{required: true, message: 'Please input Languages!'}],
                            })(
                                <Select
                                    prefix={<Icon type="user"/>}
                                    mode="multiple"
                                    allowClear
                                    placeholder="Languages">
                                    {
                                        _.map(languages, ({_id, name}) => (
                                            <Select.Option key={_id} value={_id}>{name}</Select.Option>
                                        ))
                                    }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="Parent Page">
                            {getFieldDecorator('parentId', {
                                initialValue: data.parentId,
                            })(
                                <Select
                                    prefix={<Icon type="user"/>}
                                    placeholder="Page">
                                    {
                                        _.map(pages, ({_id, title}) => (
                                            <Select.Option key={_id} value={_id}>{title}</Select.Option>
                                        ))
                                    }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="Post List in Layouts">
                            {getFieldDecorator('postLayoutIds', {
                                initialValue: data.postLayoutIds,
                            })(
                                <Select
                                    prefix={<Icon type="user"/>}
                                    mode="multiple"
                                    allowClear
                                    placeholder="Please select layouts"
                                >
                                    {
                                        _.map(pages, ({_id, title}) => (
                                            <Select.Option key={_id} value={_id}>{title}</Select.Option>
                                        ))
                                    }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item label="Page Contents">
                            {getFieldDecorator('contentIds', {
                                initialValue: data.contentIds,
                            })(
                                <Select
                                    prefix={<Icon type="deployment-unit"/>}
                                    mode="multiple"
                                    allowClear
                                    style={{width: '100%'}}
                                    placeholder="Please select contents"
                                >
                                    {
                                        _.map(contents, ({_id, title}) => (
                                            <Select.Option key={_id} value={_id}>{title}</Select.Option>
                                        ))
                                    }
                                </Select>,
                            )}
                        </Form.Item>
                        <Form.Item label="Order (number)">
                            {getFieldDecorator('order', {
                                initialValue: data.order,
                            })(
                                <Input
                                    type="number"
                                    placeholder="Order"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator(`shownMenu`, {
                                initialValue: !!data.shownMenu,
                                valuePropName: 'checked'
                            })(
                                <Switch
                                    unCheckedChildren="Hide in menu"
                                    checkedChildren="Show in menu"
                                />
                            )}
                        </Form.Item>
                        <Form.Item label="Keywords">
                            {getFieldDecorator('keywords', {
                                initialValue: data.keywords || [],
                            })(
                                <TagItems/>
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="save-button">
                                Save
                            </Button>
                        </Form.Item>
                    </Card>
                </Col>
            </Row>
        </Form>
    );
};

export default withForm(PageForm);
