import React from 'react';
import {Route} from 'react-router-dom';

import LoginRedirect from "../../components/LoginRedirect";
import SideMenuLayout from "../side-menu-layout";
import isRedirect from "./isRedirect";

const SideMenuLayoutRoute = ({isPublic, component: Component, ...rest}) => {
    return (
        <Route
            {...rest}
            render={matchProps => {

                if (isRedirect(isPublic)) {
                    return (<LoginRedirect/>);
                }

                return (
                    <SideMenuLayout>
                        <Component {...matchProps} />
                    </SideMenuLayout>
                );
            }}
        />
    )
};


export default SideMenuLayoutRoute;
