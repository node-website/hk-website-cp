/*
* GeneralProvider creates all the contexts needed by Venuex and wraps with provider
*/
import React, {createContext, Suspense} from "react";
import PropTypes from "prop-types";
import {AuthProvider} from "./AuthContext";
import {BrowserRouter as Router} from "react-router-dom";


export const RootContext = createContext();

const Loader = () => (
    <div className="fallback-loading">
        <span className="loader-animation"/>
    </div>
);

export const RootProvider = ({children}) => {
    return (
        <RootContext.Provider>
            <Suspense fallback={<Loader/>}>
                <Router basename={process.env.PUBLIC_URL}>
                    <AuthProvider>
                        {children}
                    </AuthProvider>
                </Router>
            </Suspense>
        </RootContext.Provider>
    );
};

RootContext.propTypes = {
    children: PropTypes.node.isRequired
};
