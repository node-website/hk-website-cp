import React from 'react';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import PartialForm from "../form";

import './style.scss';
import {withRouter} from "react-router-dom";
import api from "../../../../services/api";

const {Content} = Layout;

const routes = [
    {
        path: '/layouts/partials',
        text: 'Partials',
    },
    {
        path: '/layouts/partials/add',
        text: 'New Partial',
    },
];

const AddPartial = ({history}) => {

    const onSubmit = async (values) => {
        try {
            await api.post("/partials", {...values});

            message.success("Save Success");

            setTimeout(history.goBack, 3000);
        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    return (
        <>
            <AppPageHeader
                title="New Partial"
                breadcrumb={routes}
            />

            <Content className="page-content add-language">

                <PartialForm onSubmit={onSubmit}/>
            </Content>
        </>
    );
};

export default withRouter(AddPartial);
