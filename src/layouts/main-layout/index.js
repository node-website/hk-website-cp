import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import * as _ from "lodash";
import {Icon, Layout, Menu} from 'antd';

import menus from "./menus";

import './style.scss';

const {Header} = Layout;

const MainLayout = ({children, location}) => {

    const {pathname} = location;

    return (

        <Layout className="parent-layout">
            <Header style={{position: 'fixed', zIndex: 1, width: '100%'}}>
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={[pathname]}
                    style={{lineHeight: '64px'}}
                >
                    {
                        _.map(menus, (menu) => (
                            <Menu.Item key={menu.path}>
                                <Link to={menu.path}>
                                    {menu.icon && <Icon type={menu.icon}/>}
                                    <span className="nav-text">{menu.text}</span>
                                </Link>
                            </Menu.Item>
                        ))
                    }
                </Menu>
            </Header>
            <Layout
                style={{
                    marginTop: 64,
                    height: '100vh',
                    overflow: 'auto'
                }}
            >
                {children}
            </Layout>
        </Layout>

    );
};

export default withRouter(MainLayout);
