import React, {useEffect, useState} from 'react';
import './style.scss';
import {Layout, message} from 'antd';
import AppPageHeader from "../../../../components/AppPageHeader";
import StyleForm from "../form";
import api from "../../../../services/api";
import {withRouter} from "react-router-dom";

const {Content} = Layout;

const routes = [
  {
    path: '/layouts/styles',
    text: 'Styles',
  },
];

const EditStyle = ({match, history}) => {

  const {id} = match.params;
  const [style, setStyle] = useState();

  const getStyle = async () => {
    try {

      const data = await api.get(`/styles/${id}`);

      setStyle(data.style);

    }
    catch (e) {

      message.error(e.error || "Unknown Error");
    }
  };

  useEffect(() => {

    getStyle();

  }, []);

  const onSubmit = async (values) => {

    console.log('onSubmit.values:', values);
    try {
      await api.patch(`/styles/${id}`, {...values});

      message.success("Save Success");

      setTimeout(history.goBack, 3000);
    }
    catch (e) {

      console.log('e:', e);

      message.error(e.error || "Unknown Error");
    }
  };

  return (
    <>
      <AppPageHeader
        title={`Editing Style: ${style && style.styleName}`}
        breadcrumb={[...routes, {
          path: `/layouts/styles/${id}/edit`,
          text: 'Style Edit',
        }]}
      />

      <Content className="language-content edit-language">
        {
          style &&
          <StyleForm data={style} onSubmit={onSubmit}/>
        }
      </Content>
    </>
  );
};

export default withRouter(EditStyle);
