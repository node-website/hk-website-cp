import React from "react";
import {Breadcrumb, Icon} from 'antd';
import {Link} from "react-router-dom";
import * as _ from "lodash";

import './style.scss'

const AppPageHeader = ({title, subtitle, breadcrumb, tags, buttons}) => {

    return (
        <div className="ant-page-header has-breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link to="/"> <Icon type="home"/> Home</Link>
                </Breadcrumb.Item>
                {
                    _.map(breadcrumb, (route, index) => (
                        <Breadcrumb.Item key={route.path}>
                            {
                                breadcrumb.length === (index + 1)
                                    ? route.text
                                    : <Link to={route.path}>{route.text}</Link>
                            }
                        </Breadcrumb.Item>
                    ))
                }
            </Breadcrumb>
            <div className="ant-page-header-heading">
                <span className="ant-page-header-heading-title">{title}</span>
                <span className="ant-page-header-heading-sub-title">{subtitle}</span>

                <span className="ant-page-header-heading-tags">
                    {
                        _.map(tags, (tag) => (
                            <span
                                className={`ant-tag ant-tag-${tag.color || 'blue'}`}>
                                {tag.text}
                            </span>
                        ))
                    }
                </span>
                <span className="ant-page-header-heading-extra">
                    {buttons}
                </span>
            </div>
        </div>
    );
};

// export default withRouter(AppPageHeader);
export default AppPageHeader;
