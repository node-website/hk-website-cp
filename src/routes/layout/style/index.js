import React, {useEffect, useState} from 'react';
import {Button, Card, Layout, message, Popconfirm, Table} from 'antd';
import {Link} from "react-router-dom";
import AppPageHeader from "../../../components/AppPageHeader";
import api from "../../../services/api";

import './style.scss';

const {Content} = Layout;

const routes = [
    {
        path: '/layouts/styles',
        text: 'Styles',
    },
];

const Styles = () => {

    const [styles, setStyles] = useState([]);

    const getStyles = async () => {
        try {

            const data = await api.get("/styles");

            setStyles(data.styles);

        } catch (e) {

            message.error(e.error || "Unknown Error");
        }
    };

    const onDeleteStyles = (id) => async () => {
        try {
            await api.delete(`/styles/${id}`);

            message.success("Delete Success");

            getStyles();
        } catch (e) {
            message.error(e.error || "Unknown Error");
        }
    };

    useEffect(() => {

        getStyles();

    }, []);

    return (
        <>
            <AppPageHeader
                breadcrumb={routes}
                title="Layouts"
                subtitle="Styles Editor"
            />

            <Content className="page-content styles">
                <Card
                    bordered={false}
                    title="Styles"
                    extra={
                        <>
                            <Link to="/layouts/styles/add">
                                <Button type="primary" ghost size="small" icon="plus"/>
                            </Link>
                        </>
                    }
                >
                    <Table
                        dataSource={styles}
                        rowKey="_id"
                        size="small"
                        bordered={true}
                    >
                        <Table.Column
                            title="Style Name"
                            dataIndex="styleName"
                            key="styleName"
                        />
                        <Table.Column
                            title="How To Use"
                            dataIndex="styleName"
                            key="styleNameUse"
                            render={(styleName) => (<code>{`<link rel="stylesheet" type="text/css" href="/styles/${styleName}.css"/>`}</code>)}
                        />
                        <Table.Column
                            title="Actions"
                            dataIndex="_id"
                            key="_id"
                            width={90}
                            align="center"
                            render={(id) => (
                                <div className="actions">
                                    <Link to={`/layouts/styles/${id}/edit`}>
                                        <Button type="primary" ghost size="small" icon="edit"/>
                                    </Link>
                                    <Popconfirm
                                        placement="bottomRight"
                                        title="Delete this content?"
                                        onConfirm={onDeleteStyles(id)}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <Button type="danger" ghost size="small" icon="delete"/>
                                    </Popconfirm>
                                </div>
                            )}
                        />
                    </Table>
                </Card>
            </Content>
        </>
    );
};

export default Styles;
